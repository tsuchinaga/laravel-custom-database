<?php

namespace App\Services;

use App\Models\AuthUser;
use App\Models\Tenant\User;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InitializeDatabaseService
{
    public function __construct(
        private ?DatabaseConnectionService $databaseConnectionService = null,
    )
    {
    }

    public function initialize(string $database_name, ?AuthUser $user = null): void
    {
        Schema::createDatabase($database_name);
        $this->databaseConnectionService->config($database_name, $database_name);

        // テーブルの用意
        $this->createUsersTable($database_name);
        $this->createCustomersTable($database_name);

        // 初期ユーザの追加
        if (!is_null($user)) {
            $tenant_user = new User([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'is_tenant_manager' => true,
            ]);
            $tenant_user->setConnection($database_name);
            $tenant_user->save();
        }
    }

    private function createUsersTable(string $connection_name = 'tenant'): void
    {
        Schema::connection($connection_name)->create('users', function (Blueprint $table) {
            $table->integer('id');
            $table->string('name');
            $table->string('email');
            $table->boolean('is_tenant_manager')->default(false);
            $table->timestamp("created_at")->default(DB::raw("current_timestamp"));
            $table->timestamp("updated_at")->default(DB::raw("current_timestamp"));
        });
    }

    private function createCustomersTable(string $connection_name = 'tenant'): void
    {
        Schema::connection($connection_name)->create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('address');
            $table->string('tel');
            $table->timestamp("created_at")->default(DB::raw("current_timestamp"));
            $table->timestamp("updated_at")->default(DB::raw("current_timestamp"));
        });
    }
}
