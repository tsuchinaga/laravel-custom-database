<?php

namespace App\Services;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseConnectionService
{
    public function config(string $database_name, string $connection_name = 'tenant'): void
    {
        $connection = config('database.connections.' . env('DB_CONNECTION'));
        $connection['database'] = $database_name;
        config(["database.connections.$connection_name" => $connection]);
    }
}
