<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AuthUser extends Authenticatable
{
    use HasFactory;

    protected $connection;

    protected $table = "users";

    protected $guarded = [];

    public $timestamps = false;

    public function verify(string $password): bool
    {
        return password_verify($password, $this->password);
    }
}
