<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;

    protected $table = "tenants";

    protected $guarded = [];

    public $timestamps = false;

    public function databaseName(): string
    {
        return config('const.TENANT_DB_PREFIX') . $this->database_name;
    }
}
