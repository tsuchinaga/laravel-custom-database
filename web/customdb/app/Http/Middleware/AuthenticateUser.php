<?php

namespace App\Http\Middleware;

use App\Models\Tenant;
use App\Models\AuthUser;
use App\Services\DatabaseConnectionService;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = AuthUser::query()->where('id', '=', $request->session()->get(config('const.SESSION_USER_ID')))->first();
        if (is_null($user)) return redirect('/login')->with(['message' => 'ログインしてください']);

        Auth::setUser($user);

        // 接続するテナントDBのコネクションを作っておく
        $tenant = Tenant::query()->where('id', '=', $user->tenant_id)->first();
        if (is_null($tenant)) return redirect('/login')->with(['message' => 'ログインしてください']);;

        $connector = new DatabaseConnectionService();
        $connector->config($tenant->databaseName());

        return $next($request);
    }
}
