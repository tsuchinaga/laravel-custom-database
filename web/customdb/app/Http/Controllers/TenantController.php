<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use App\Models\AuthUser;
use App\Services\InitializeDatabaseService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class TenantController extends Controller
{

    public function __construct(
        public ?InitializeDatabaseService $initializeDatabaseService = null
    )
    {
    }

    public function list(Request $request): Factory|View|Application
    {
        $tenants = Tenant::query()->orderBy('id')->get();

        return view('tenant.list', [
            'tenants' => $tenants,
        ]);
    }

    public function registerForm(Request $request): Factory|View|Application
    {
        return view('tenant.register', []);
    }

    public function register(Request $request): Redirector|Application|RedirectResponse
    {
        $name = $request->get('name');
        $database_name = $request->get('database_name');

        $tenant = new Tenant(['name' => $name, 'database_name' => $database_name]);
        $tenant->save();

        $auth_user = new AuthUser([
            'name' => $request->get('manager_name'),
            'email' => $request->get('manager_email'),
            'password' => password_hash($request->get('manager_password'), PASSWORD_DEFAULT),
            'tenant_id' => $tenant->id,
        ]);
        $auth_user->save();

        $this->initializeDatabaseService->initialize($tenant->databaseName(), $auth_user);




        return redirect('/tenants');
    }
}
