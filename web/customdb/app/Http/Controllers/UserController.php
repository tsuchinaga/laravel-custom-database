<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\AuthUser;
use App\Models\Tenant\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function login(): Factory|View|Application
    {
        return view('login');
    }

    public function execLogin(LoginRequest $request): Redirector|Application|RedirectResponse
    {
        $user = AuthUser::query()->where('email', '=', $request->get('email', ''))->first();
        if (is_null($user) || !$user->verify($request->get('password', ''))) return back()->withInput()->with(['message' => '認証に失敗しました']);

        // ログイン済みの情報をsessionに保存
        session([
            config('const.SESSION_USER_ID') => $user->id,
            config('const.SESSION_TENANT_ID') => $user->tenant_id,
        ]);
        return redirect('/');
    }

    public function menu(): Factory|View|Application
    {
        return view('user.menu', ['user' => Auth::user()]);
    }

    public function list(Request $request): Factory|View|Application
    {
        $users = User::query()->orderBy('id')->get();

        return view('user.list', [
            'users' => $users,
        ]);
    }

    public function registerForm(Request $request): Factory|View|Application
    {
        return view('user.register', []);
    }

    public function register(Request $request): Redirector|Application|RedirectResponse
    {
        $login_user = Auth::user();

        $name = $request->get('name');
        $email = $request->get('email');
        $password = $request->get('password');

        $master_user = new AuthUser(['name' => $name, 'email' => $email, 'password' => password_hash($password, PASSWORD_DEFAULT), 'tenant_id' => $login_user->tenant_id]);
        $master_user->save();

        $tenant_user = new User(['id' => $master_user->id, 'name' => $name, 'email' => $email]);
        $tenant_user->save();

        return redirect('/users');
    }
}
