<?php

namespace App\Http\Controllers;

use App\Models\Tenant\Customer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;

class CustomerController extends Controller
{

    public function list(Request $request): Factory|View|Application
    {
        $customers = Customer::query()->orderBy('id')->get();

        return view('customer.list', [
            'customers' => $customers,
        ]);
    }

    public function registerForm(Request $request): Factory|View|Application
    {
        return view('customer.register', []);
    }

    public function register(Request $request): Redirector|Application|RedirectResponse
    {
        $name = $request->get('name');
        $address = $request->get('address');
        $tel = $request->get('tel');

        Customer::query()->insert(['name' => $name, 'address' => $address, 'tel' => $tel]);
        return redirect('/customers');
    }
}
