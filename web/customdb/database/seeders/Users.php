<?php

namespace Database\Seeders;

use App\Models\AuthUser;
use App\Models\Tenant;
use App\Models\Tenant\User;
use App\Services\DatabaseConnectionService;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataset = [
            ['tenant_id' => 1, 'name' => 'user-name-001', 'email' => 'user001@example.com', 'password' => password_hash('user001', PASSWORD_DEFAULT), 'is_manager' => true],
            ['tenant_id' => 1, 'name' => 'user-name-002', 'email' => 'user002@example.com', 'password' => password_hash('user002', PASSWORD_DEFAULT), 'is_manager' => true],
            ['tenant_id' => 1, 'name' => 'user-name-003', 'email' => 'user003@example.com', 'password' => password_hash('user003', PASSWORD_DEFAULT), 'is_manager' => true],
            ['tenant_id' => 2, 'name' => 'user-name-004', 'email' => 'user004@example.com', 'password' => password_hash('user004', PASSWORD_DEFAULT), 'is_manager' => false],
            ['tenant_id' => 3, 'name' => 'user-name-005', 'email' => 'user005@example.com', 'password' => password_hash('user005', PASSWORD_DEFAULT), 'is_manager' => false],
        ];

        $connector = new DatabaseConnectionService();
        foreach ($dataset as $data) {
            $user = new AuthUser($data);
            $user->save();

            // テナントユーザも追加
            $tenant = Tenant::query()->where('id', '=', $user->tenant_id)->first();
            $connector->config($tenant->databaseName(), $tenant->databaseName());

            $tenant_user = new User([
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
            ]);
            $tenant_user->setConnection($tenant->databaseName());
            $tenant_user->save();
        }
    }
}
