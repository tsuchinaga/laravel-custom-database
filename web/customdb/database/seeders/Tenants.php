<?php

namespace Database\Seeders;

use App\Models\AuthUser;
use App\Models\Tenant;
use App\Services\DatabaseConnectionService;
use App\Services\InitializeDatabaseService;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class Tenants extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dataset = [
            ['name' => 'tenant-name-001', 'database_name' => '001'],
            ['name' => 'tenant-name-002', 'database_name' => '002'],
            ['name' => 'tenant-name-003', 'database_name' => '003'],
        ];

        $initialize_database_service = new InitializeDatabaseService(databaseConnectionService: new DatabaseConnectionService());
        foreach ($dataset as $data) {
            $tenant = new Tenant($data);
            $tenant->save();

            // 初期ユーザの追加
            $user = new AuthUser([
                'tenant_id' => $tenant->id,
                'name' => $data['name'],
                'email' => "{$data['name']}@example.com",
                'password' => password_hash($data['name'], PASSWORD_DEFAULT),
                'is_manager' => false,
            ]);
            $user->save();

            // database
            Schema::dropDatabaseIfExists($tenant->databaseName());
            $initialize_database_service->initialize($tenant->databaseName(), $user);
        }
    }
}
