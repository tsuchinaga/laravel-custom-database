<?php

return [
    'SESSION_USER_ID' => 'auth.user_id',
    'SESSION_TENANT_ID' => 'auth.tenant_id',

    'TENANT_DB_PREFIX' => 'tenant_',
];
