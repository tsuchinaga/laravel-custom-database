@extends('layouts.base')
@section('content')

    <a href="{{url('/')}}">Back</a>
    <a href="{{url('/customers/register')}}">New</a>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Address</td>
            <td>Tel</td>
        </tr>
        </thead>
        <tbody>
        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->name}}</td>
                <td>{{$customer->address}}</td>
                <td>{{$customer->tel}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
