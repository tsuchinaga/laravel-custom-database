@extends('layouts.base')
@section('content')

    <form method="post" action="{{url('/customers/register')}}">
        @csrf

        <div>
            <label for="name">Name</label>
            <input id="name" type="text" name="name" required>
        </div>
        <div>
            <label for="address">Address</label>
            <input id="address" type="text" name="address" required>
        </div>
        <div>
            <label for="tel">Tel</label>
            <input id="tel" type="tel" name="tel" required>
        </div>
        <button type="submit">submit</button>
    </form>

@endsection
