@extends('layouts.base')
@section('content')

    @if(session('message'))
        <div>
            <span style="color: red; font-weight: bold;">{{session('message')}}</span>
        </div>
    @endif

    <form method="post" action="{{url('/login')}}">
        @csrf

        <div>
            <label for="email">Email</label>
            <input id="email" type="email" name="email" value="{{old('email')}}" required>
            @error('email')
            <span style="color: red; font-weight: bold;">{{$message}}</span>
            @enderror
        </div>
        <div>
            <label for="password">Password</label>
            <input id="password" type="password" name="password" required>
            @error('password')
            <span style="color: red; font-weight: bold;">{{$message}}</span>
            @enderror
        </div>

        <button type="submit">submit</button>
    </form>

@endsection
