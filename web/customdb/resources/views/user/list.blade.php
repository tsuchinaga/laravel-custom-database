@extends('layouts.base')
@section('content')

    <a href="{{url("/users/register")}}">New</a>
    <a href="{{url("/")}}">Back</a>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
