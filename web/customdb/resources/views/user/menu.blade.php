@extends('layouts.base')
@section('content')

    <ul>
        @if($user->is_manager)
            <li><a href="{{url('/tenants')}}">テナント管理</a></li>
        @endif
        <li><a href="{{url('/users')}}">ユーザ管理</a></li>
        <li><a href="{{url('/customers')}}">顧客管理</a></li>
    </ul>

@endsection
