@extends('layouts.base')
@section('content')

    <form method="post" action="{{url("/users/register")}}">
        @csrf

        <div>
            <label for="name">Name</label>
            <input id="name" type="text" name="name">
        </div>
        <div>
            <label for="email">Email</label>
            <input id="email" type="email" name="email">
        </div>
        <div>
            <label for="password">Password</label>
            <input id="password" type="password" name="password">
        </div>
        <button type="submit">submit</button>
    </form>

@endsection
