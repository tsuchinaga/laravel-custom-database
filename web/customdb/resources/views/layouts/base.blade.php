<!doctype html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>カスタムDB</title>
</head>
<body>

<main>
    <a href="{{url('/logout')}}">Logout</a>
    @yield('content')
</main>

</body>
</html>
