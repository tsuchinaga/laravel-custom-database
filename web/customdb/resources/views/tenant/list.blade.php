@extends('layouts.base')
@section('content')

    <a href="{{url('/')}}">Back</a>
    <a href="{{url('/tenants/register')}}">New</a>
    <table>
        <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Database Name</td>
        </tr>
        </thead>
        <tbody>
        @foreach($tenants as $tenant)
            <tr>
                <td>{{$tenant->id}}</td>
                <td>{{$tenant->name}}</td>
                <td>{{$tenant->databaseName()}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
