@extends('layouts.base')
@section('content')

    <form method="post" action="{{url('/tenants/register')}}">
        @csrf

        <div>
            <label for="name">Name</label>
            <input id="name" type="text" name="name" required>
        </div>
        <div>
            <label for="database_name">Database Name</label>
            <input id="database_name" type="text" name="database_name" required>
        </div>
        <div>
            <label for="manager_name">Manager Name</label>
            <input id="manager_name" type="text" name="manager_name" required>
        </div>
        <div>
            <label for="manager_email">Manager Email</label>
            <input id="manager_email" type="text" name="manager_email" required>
        </div>
        <div>
            <label for="manager_password">Manager Password</label>
            <input id="manager_password" type="text" name="manager_password" required>
        </div>
        <button type="submit">submit</button>
    </form>

@endsection
