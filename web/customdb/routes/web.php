<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login', [UserController::class, 'login'])->name('login');
Route::post('/login', [UserController::class, 'execLogin']);

Route::group(['middleware' => ['auth.user']], function () {
    Route::get('/', [UserController::class, 'menu']);

    Route::prefix('/tenants')->group(function () {
        Route::get('', [TenantController::class, 'list']);
        Route::get('/register', [TenantController::class, 'registerForm']);
        Route::post('/register', [TenantController::class, 'register']);
    });

    Route::prefix('/users')->group(function () {
        Route::get('', [UserController::class, 'list']);
        Route::get('/register', [UserController::class, 'registerForm']);
        Route::post('/register', [UserController::class, 'register']);
    });

    Route::prefix('/customers')->group(function () {
        Route::get('', [CustomerController::class, 'list']);
        Route::get('/register', [CustomerController::class, 'registerForm']);
        Route::post('/register', [CustomerController::class, 'register']);
    });


    Route::get('/logout', function () {
        session()->flush();
        return redirect('/login');
    });
});
